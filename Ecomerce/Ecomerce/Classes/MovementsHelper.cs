﻿using Ecomerce.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ecomerce.Classes
{
    public class MovementsHelper : IDisposable
    {
        private static EcomerceContext db = new EcomerceContext();
        public void Dispose()
        {
            db.Dispose();
        }

        public static Response NewOrder(NewOrderView view, string UserName)
        {
            using (var transacction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.Users.Where(u => u.UserName == UserName).FirstOrDefault();
                    var order = new Order
                    {
                        CompanyId = user.CompanyId,
                        CustomerId = view.CustomerId,
                        Date = view.Date,
                        Remarks = view.Remarks,
                        StateId = DBHelper.GetState("Created", db),
                    };

                    db.Orders.Add(order);
                    db.SaveChanges();
                    var details = db.OrderDetailTmps.Where(odt => odt.UserName == UserName).ToList();


                    foreach (var detail in details)
                    {
                        var orderDetail = new OrderDetail
                        {
                            Description = detail.Description,
                            OrderId = order.OrderId,
                            Price = detail.Price,
                            ProductId = detail.ProductId,
                            Quantity = detail.Quantity,
                            TaxRate = detail.TaxRate,
                        };

                        db.OrderDetails.Add(orderDetail);
                        db.OrderDetailTmps.Remove(detail);
                    }

                    db.SaveChanges();
                    transacction.Commit();

                    return new Response { Succeeded = true, };
                }
                catch (Exception Ex)
                {
                    transacction.Rollback();
                    return new Response
                    {
                        Succeeded = false,
                        Message = Ex.Message,
                    };
                }
            }
        }

        public static Response NewPurchase(NewPurchaseView view, string UserName)
        {
            using (var transacction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.Users.Where(u => u.UserName == UserName).FirstOrDefault();
                    var purchase = new Purchase
                    {
                        CompanyId = user.CompanyId,
                        SupplierId = view.SupplierId,
                        StateId = DBHelper.GetState("Created", db),
                        Date = view.Date,
                        Remarks = view.Remarks,
                        WarehouseId = view.WarehouseId,
                    };

                    db.Purchases.Add(purchase);
                    db.SaveChanges();
                    var details = db.PurchaseDetailTmps.Where(pdt => pdt.UserName == UserName).ToList();

                    foreach (var detail in details)
                    {
                        var purchaseDetail = new PurchaseDetail
                        {
                            PurchaseId = purchase.PurchaseId,
                            ProductId = detail.ProductId,
                            Description = detail.Description,
                            TaxRate = detail.TaxRate,
                            Cost = detail.Cost,
                            Quantity = detail.Quantity,
                        };
                        db.PurchaseDetails.Add(purchaseDetail);
                        db.PurchaseDetailTmps.Remove(detail);

                        var inventories = db.Inventories.Where(i => i.WarehouseId == view.WarehouseId && i.ProductId == detail.ProductId).FirstOrDefault();
                        if (inventories == null)
                        {
                            var inventori = new Inventory
                            {
                                WarehouseId = view.WarehouseId,
                                ProductId = detail.ProductId,
                                Stock = detail.Quantity,
                            };
                            db.Inventories.Add(inventori);
                        }
                        else
                        {
                            inventories.Stock += detail.Quantity;
                            db.Entry(inventories).State = EntityState.Modified;
                        }
                    }

                    db.SaveChanges();
                    transacction.Commit();

                    return new Response { Succeeded = true, };
                }
                catch (Exception Ex)
                {
                    transacction.Rollback();
                    return new Response
                    {
                        Succeeded = false,
                        Message = Ex.Message,
                    };
                }
            }
        }

        public static Response NewSale(NewSaleView view, string UserName)
        {
            using (var transacction = db.Database.BeginTransaction())
            {
                try
                {
                    var user = db.Users.Where(u => u.UserName == UserName).FirstOrDefault();
                    var order = db.SaleDetailTmps.Where(sdt => sdt.UserName == user.UserName && sdt.OrderId != 0).FirstOrDefault();
                    var sale = new Sale
                    {
                        CompanyId = user.CompanyId,
                        CustomerId = view.CustomerId,
                        WarehouseId = view.WarehouseId,
                        StateId = DBHelper.GetState("Created", db),
                        Date = view.Date,
                        Remarks = view.Remarks,
                        OrderId = order == null ? 0 : order.OrderId,
                    };

                    db.Sales.Add(sale);
                    db.SaveChanges();

                    if (order != null && order.OrderId != 0)
                    {
                        var ord = db.Orders.Find(order.OrderId);
                        ord.StateId = DBHelper.GetState("Invoiced", db);
                        db.Entry(ord).State = EntityState.Modified;
                        db.SaveChanges();
                    }


                    var details = db.SaleDetailTmps.Where(pdt => pdt.UserName == UserName).ToList();

                    foreach (var detail in details)
                    {
                        var saleDetail = new SaleDetail
                        {
                            SaleId = sale.SaleId,
                            ProductId = detail.ProductId,
                            Description = detail.Description,
                            TaxRate = detail.TaxRate,
                            Price = detail.Price,
                            Quantity = detail.Quantity,
                        };
                        db.SaleDetails.Add(saleDetail);
                        db.SaleDetailTmps.Remove(detail);

                        var inventories = db.Inventories.Where(i => i.WarehouseId == view.WarehouseId && i.ProductId == detail.ProductId).FirstOrDefault();
                        if (inventories == null)
                        {
                            var inventori = new Inventory
                            {
                                WarehouseId = view.WarehouseId,
                                ProductId = detail.ProductId,
                                Stock = -detail.Quantity,
                            };
                            db.Inventories.Add(inventori);
                        }
                        else
                        {
                            inventories.Stock -= detail.Quantity;
                            db.Entry(inventories).State = EntityState.Modified;
                        }
                    }

                    db.SaveChanges();
                    transacction.Commit();

                    return new Response { Succeeded = true, };
                }
                catch (Exception Ex)
                {
                    transacction.Rollback();
                    return new Response
                    {
                        Succeeded = false,
                        Message = Ex.Message,
                    };
                }
            }
        }
    }
}