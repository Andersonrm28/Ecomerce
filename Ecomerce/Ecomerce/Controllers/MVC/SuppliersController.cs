﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ecomerce.Models;
using PagedList;
using Ecomerce.Classes;

namespace Ecomerce.Controllers
{
    [Authorize(Roles ="User")]
    public class SuppliersController : Controller
    {
        private EcomerceContext db = new EcomerceContext();

        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var qry = (from cu in db.Suppliers
                       join sc in db.SupplierCompanies on cu.SupplierId equals sc.SupplierId
                       join co in db.Companies on sc.CompanyId equals co.CompanyId
                       where co.CompanyId == user.CompanyId
                       select new { cu }).ToList();

            var suppliers = new List<Supplier>();
            foreach (var item in qry)
            {
                suppliers.Add(item.cu);
            }
            return View(suppliers.ToList().OrderBy(s => s.FirstName).ThenBy(s => s.LastName).ToPagedList((int)page, 5));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(0), "CityId", "Name");
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.Suppliers.Add(supplier);
                        var response = DBHelper.SaveChanges(db);
                        if (!response.Succeeded)
                        {
                            ModelState.AddModelError(string.Empty, response.Message);
                            transaction.Rollback();
                            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
                            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", supplier.DepartmentId);
                            return View(supplier);
                        }
                        UsersHelper.CreateUserASP(supplier.UserName, "Supplier");

                        var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
                        var supplierCompany = new SupplierCompany
                        {
                            CompanyId = user.CompanyId,
                            SupplierId = supplier.SupplierId,
                        };
                        db.SupplierCompanies.Add(supplierCompany);
                        db.SaveChanges();

                        if (supplier.PhotoFile != null)
                        {
                            var folder = "~/Content/Suppliers";
                            var file = string.Format("{0}.jpg", supplier.SupplierId);
                            var respon = FilesHelper.UploadPhoto(supplier.PhotoFile, folder, file);
                            if (respon)
                            {
                                supplier.Photo = string.Format("{0}/{1}", folder, file);
                                db.Entry(supplier).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception Ex)
                    {
                        transaction.Rollback();
                        ModelState.AddModelError(string.Empty, Ex.Message);
                    }
                }
            }

            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", supplier.DepartmentId);
            return View(supplier);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", supplier.DepartmentId);
            return View(supplier);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                var db2 = new EcomerceContext();
                var user = db2.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
                var currentUser = db2.Users.Find(user.UserId);
                if (currentUser.UserName != supplier.UserName)
                {
                    UsersHelper.UpdateUserName(currentUser.UserName, supplier.UserName);
                }
                db2.Dispose();
                db.Entry(supplier).State = EntityState.Modified;
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    if (supplier.PhotoFile != null)
                    {
                        var folder = "~/Content/Suppliers";
                        var file = string.Format("{0}.jpg", supplier.SupplierId);
                        var respon = FilesHelper.UploadPhoto(supplier.PhotoFile, folder, file);
                        if (respon)
                        {
                            supplier.Photo = string.Format("{0}/{1}", folder, file);
                            db.Entry(supplier).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, response.Message);
            }
            ViewBag.CityId = new SelectList(CombosHelper.GetCities(supplier.DepartmentId), "CityId", "Name", supplier.CityId);
            ViewBag.DepartmentId = new SelectList(CombosHelper.GetDepartments(), "DepartmentId", "Name", supplier.DepartmentId);
            return View(supplier);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var supplier = db.Suppliers.Find(id);
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var supplierCompany = db.SupplierCompanies.Where(cc => cc.CompanyId == user.CompanyId && cc.SupplierId == supplier.SupplierId).FirstOrDefault();

            using (var transaction = db.Database.BeginTransaction())
            {
                db.SupplierCompanies.Remove(supplierCompany);
                db.Suppliers.Remove(supplier);
                var response = DBHelper.SaveChanges(db);

                if (response.Succeeded)
                {
                    UsersHelper.DeleteUser(supplier.UserName, "Supplier");
                    transaction.Commit();
                    return RedirectToAction("Index");
                }

                transaction.Rollback();
                ModelState.AddModelError(string.Empty, response.Message);
                db.Entry(supplier).State = EntityState.Added;
                return View(supplier);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
