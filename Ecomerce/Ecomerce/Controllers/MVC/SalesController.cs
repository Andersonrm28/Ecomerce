﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ecomerce.Classes;
using Ecomerce.Models;
using PagedList;

namespace Ecomerce.Controllers
{
    [Authorize(Roles = "User")]
    public class SalesController : Controller
    {
        private EcomerceContext db = new EcomerceContext();

        public ActionResult AddProduct()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId, true), "ProductId", "Description");
            return PartialView();
        }

        public ActionResult AddOrder()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.OrderId = new SelectList(CombosHelper.GetOrders(user.CompanyId, true), "OrderId", "OrderId");
            return PartialView();
        }

        [HttpPost]
        public ActionResult AddOrder(AddOrderView view)
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            if (ModelState.IsValid)
            {
                var OrderDetails = db.OrderDetails.Where(od => od.OrderId == view.OrderId).ToList();
                if (OrderDetails != null)
                {
                    var saleDt = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.OrderId != 0).ToList(); 
                    if (saleDt.Count() > 0)
                    {
                        foreach (var item in saleDt)
                        {
                            db.SaleDetailTmps.Remove(item);
                        }
                        db.SaveChanges();
                    }

                    foreach (var item in OrderDetails)
                    {
                        var saleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == item.ProductId).FirstOrDefault();
                        if (saleDetailTmp == null)
                        {
                            var product = db.Products.Find(item.ProductId);
                            saleDetailTmp = new SaleDetailTmp
                            {
                                UserName = User.Identity.Name,
                                ProductId = item.ProductId,
                                Description = product.Description,
                                TaxRate = product.Tax.Rate,
                                Price = product.Price,
                                Quantity = item.Quantity,
                                OrderId = view.OrderId,
                            };
                            db.SaleDetailTmps.Add(saleDetailTmp);
                        }
                        else
                        {
                            saleDetailTmp.OrderId = view.OrderId;
                            saleDetailTmp.Quantity += item.Quantity;
                            db.Entry(saleDetailTmp).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                    }
                    return RedirectToAction("Create");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "The order does not have details");
                }
            }
            ViewBag.OrderId = new SelectList(CombosHelper.GetOrders(user.CompanyId, true), "OrderId", "OrderId");
            return PartialView(view);
        }

        [HttpPost]
        public ActionResult AddProduct(AddProductView view)
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            if (ModelState.IsValid)
            {
                var saleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == view.ProductId).FirstOrDefault();
                if (saleDetailTmp == null)
                {
                    var product = db.Products.Find(view.ProductId);
                    saleDetailTmp = new SaleDetailTmp
                    {
                        UserName = User.Identity.Name,
                        ProductId = view.ProductId,
                        Description = product.Description,
                        TaxRate = product.Tax.Rate,
                        Price = product.Price,
                        Quantity = view.Quantity,
                    };
                    db.SaleDetailTmps.Add(saleDetailTmp);
                }
                else
                {
                    saleDetailTmp.Quantity += view.Quantity;
                    db.Entry(saleDetailTmp).State = EntityState.Modified;
                }

                db.SaveChanges();
                return RedirectToAction("Create");
            }
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId), "ProductId", "Description");
            return PartialView(view);
        }

        public ActionResult DeleteProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var saleDetailTmp = db.SaleDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

            if (saleDetailTmp == null)
            {
                return HttpNotFound();
            }

            db.SaleDetailTmps.Remove(saleDetailTmp);
            db.SaveChanges();
            return RedirectToAction("Create");
        }

        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var sales = db.Sales.Where(o => o.CompanyId == user.CompanyId).Include(s => s.Customer).Include(s => s.State).Include(s => s.Warehouse);
            return View(sales.ToList().ToPagedList((int)page, 5));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sale = db.Sales.Find(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return View(sale);
        }

        public ActionResult Create()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehouses(user.CompanyId), "WarehouseId", "Name");
            var view = new NewSaleView
            {
                Date = DateTime.Now,
                Details = db.SaleDetailTmps.Where(pdt => pdt.UserName == User.Identity.Name).ToList(),
            };
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewSaleView view)
        {
            if (ModelState.IsValid)
            {
                var details = db.SaleDetailTmps.Where(pdt => pdt.UserName == User.Identity.Name).ToList();
                if (details.Count() != 0)
                {
                    var response = MovementsHelper.NewSale(view, User.Identity.Name);
                    if (response.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, response.Message);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "There are no products");
                }
            }

            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehouses(user.CompanyId), "WarehouseId", "Name");
            view.Details = db.SaleDetailTmps.Where(pdt => pdt.UserName == User.Identity.Name).ToList();
            return View(view);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sale sale = db.Sales.Find(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.CustomerId = new SelectList(CombosHelper.GetCustomers(user.CompanyId), "CustomerId", "FullName", sale.CustomerId);
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehouses(user.CompanyId), "WarehouseId", "Name", sale.WarehouseId);
            return View(sale);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Sale sale)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sale).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", sale.CompanyId);
            ViewBag.CustomerId = new SelectList(db.Customers, "CustomerId", "UserName", sale.CustomerId);
            ViewBag.OrderId = new SelectList(db.Orders, "OrderId", "Remarks", sale.OrderId);
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", sale.StateId);
            ViewBag.WarehouseId = new SelectList(db.Warehouses, "WarehouseId", "Name", sale.WarehouseId);
            return View(sale);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sale sale = db.Sales.Find(id);
            if (sale == null)
            {
                return HttpNotFound();
            }
            return View(sale);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var sale = db.Sales.Find(id);
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var saleDetails = db.SaleDetails.Where(sd => sd.SaleId == sale.SaleId).ToList();
            var orderAssociated = db.Orders.Where(o => o.OrderId == sale.OrderId).FirstOrDefault();

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in saleDetails)
                {
                    var inventorie = db.Inventories.Where(i => i.WarehouseId == sale.WarehouseId && i.ProductId == item.ProductId).FirstOrDefault();
                    if (inventorie != null)
                    {
                        inventorie.Stock += item.Quantity;
                        db.Entry(inventorie).State = EntityState.Modified;
                    }
                    db.SaleDetails.Remove(item);
                }
                db.SaveChanges();

                if (orderAssociated != null)
                {
                    var ord = db.Orders.Find(orderAssociated.OrderId);
                    ord.StateId = DBHelper.GetState("Created", db);
                    db.Entry(ord).State = EntityState.Modified;
                    db.SaveChanges();
                }

                db.Sales.Remove(sale);
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                transaction.Rollback();
                ModelState.AddModelError(string.Empty, response.Message);
                db.Entry(sale).State = EntityState.Added;
                sale.SaleDetails = saleDetails;
                return View(sale);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
