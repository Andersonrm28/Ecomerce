﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Ecomerce.Models;
using PagedList;
using Ecomerce.Classes;

namespace Ecomerce.Controllers
{
    [Authorize(Roles = "User")]
    public class PurchasesController : Controller
    {
        private EcomerceContext db = new EcomerceContext();

        public ActionResult AddProduct()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId, true), "ProductId", "Description");
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddProduct(AddProductView view)
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            if (ModelState.IsValid)
            {
                var purchaseDetailTmp = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == view.ProductId).FirstOrDefault();
                if (purchaseDetailTmp == null)
                {
                    var product = db.Products.Find(view.ProductId);
                    purchaseDetailTmp = new PurchaseDetailTmp
                    {
                        UserName = User.Identity.Name,
                        ProductId = view.ProductId,
                        Description = product.Description,
                        TaxRate = product.Tax.Rate,
                        Cost = view.Cost,
                        Quantity = view.Quantity,
                    };
                    db.PurchaseDetailTmps.Add(purchaseDetailTmp);
                }
                else
                {
                    purchaseDetailTmp.Quantity += view.Quantity;
                    purchaseDetailTmp.Cost = view.Cost;
                    db.Entry(purchaseDetailTmp).State = EntityState.Modified;
                }

                db.SaveChanges();
                return RedirectToAction("Create");
            }
            ViewBag.ProductId = new SelectList(CombosHelper.GetProducts(user.CompanyId), "ProductId", "Description");
            return PartialView(view);
        }

        public ActionResult DeleteProduct(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var purchaseDetailTmp = db.PurchaseDetailTmps.Where(odt => odt.UserName == User.Identity.Name && odt.ProductId == id).FirstOrDefault();

            if (purchaseDetailTmp == null)
            {
                return HttpNotFound();
            }

            db.PurchaseDetailTmps.Remove(purchaseDetailTmp);
            db.SaveChanges();
            return RedirectToAction("Create");
        }

        public ActionResult Index(int? page = null)
        {
            page = (page ?? 1);
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var purchases = db.Purchases.Where(p => p.CompanyId == user.CompanyId).Include(p => p.State).Include(p => p.Supplier).Include(p => p.Warehouse);
            return View(purchases.ToList().ToPagedList((int)page, 5));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var purchase = db.Purchases.Find(id);
            if (purchase == null)
            {
                return HttpNotFound();
            }
            return View(purchase);
        }

        public ActionResult Create()
        {
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.SupplierId = new SelectList(CombosHelper.GetSuppliers(user.CompanyId), "SupplierId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehouses(user.CompanyId), "WarehouseId", "Name");
            var view = new NewPurchaseView
            {
                Date = DateTime.Now,
                Details = db.PurchaseDetailTmps.Where(pdt => pdt.UserName == User.Identity.Name).ToList(),
            };
            return View(view);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewPurchaseView view)
        {
            if (ModelState.IsValid)
            {
                var details = db.PurchaseDetailTmps.Where(pdt => pdt.UserName == User.Identity.Name).ToList();
                if (details.Count() != 0)
                {
                    var response = MovementsHelper.NewPurchase(view, User.Identity.Name);
                    if (response.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError(string.Empty, response.Message);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "There are no products");
                }
            }

            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.SupplierId = new SelectList(CombosHelper.GetSuppliers(user.CompanyId), "SupplierId", "FullName");
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehouses(user.CompanyId), "WarehouseId", "Name");
            view.Details = db.PurchaseDetailTmps.Where(pdt => pdt.UserName == User.Identity.Name).ToList();
            return View(view);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var purchase = db.Purchases.Find(id);
            if (purchase == null)
            {
                return HttpNotFound();
            }
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            ViewBag.SupplierId = new SelectList(CombosHelper.GetSuppliers(user.CompanyId), "SupplierId", "FullName", purchase.SupplierId);
            ViewBag.WarehouseId = new SelectList(CombosHelper.GetWarehouses(user.CompanyId), "WarehouseId", "Name", purchase.WarehouseId);
            return View(purchase);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Purchase purchase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(purchase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CompanyId = new SelectList(db.Companies, "CompanyId", "Name", purchase.CompanyId);
            ViewBag.StateId = new SelectList(db.States, "StateId", "Description", purchase.StateId);
            ViewBag.SupplierId = new SelectList(db.Suppliers, "SupplierId", "UserName", purchase.SupplierId);
            ViewBag.WarehouseId = new SelectList(db.Warehouses, "WarehouseId", "Name", purchase.WarehouseId);
            return View(purchase);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var purchase = db.Purchases.Find(id);
            if (purchase == null)
            {
                return HttpNotFound();
            }
            return View(purchase);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var purchase = db.Purchases.Find(id);
            var user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            var purchaseDetails = db.PurchaseDetails.Where(pd => pd.PurchaseId == purchase.PurchaseId).ToList();

            using (var transaction = db.Database.BeginTransaction())
            {
                foreach (var item in purchaseDetails)
                {
                    var inventorie = db.Inventories.Where(i => i.WarehouseId == purchase.WarehouseId && i.ProductId == item.ProductId).FirstOrDefault();
                    if (inventorie != null)
                    {
                        inventorie.Stock -= item.Quantity;
                        db.Entry(inventorie).State = EntityState.Modified;
                    }
                    db.PurchaseDetails.Remove(item);
                }
                db.SaveChanges();

                db.Purchases.Remove(purchase);
                var response = DBHelper.SaveChanges(db);
                if (response.Succeeded)
                {
                    transaction.Commit();
                    return RedirectToAction("Index");
                }
                transaction.Rollback();
                ModelState.AddModelError(string.Empty, response.Message);
                db.Entry(purchase).State = EntityState.Added;
                purchase.PurchaseDetails = purchaseDetails;
                return View(purchase);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
