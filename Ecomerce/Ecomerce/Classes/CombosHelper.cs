﻿using Ecomerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;

namespace Ecomerce.Classes
{
    public class CombosHelper : IDisposable
    {

        private static EcomerceContext db = new EcomerceContext();
        public static List<Department> GetDepartments()
        {
            var departments = (db.Departments.ToList());
            departments.Add(new Department { DepartmentId = 0, Name = "[Select a department...]" });
            return departments.OrderBy(c => c.Name).ToList();
        }

        public static List<City> GetCities(int departmentId)
        {
            var cities = (db.Cities.Where(c => c.DepartmentId == departmentId).ToList());
            cities.Add(new City { CityId = 0, Name = "[Select a city...]" });
            return cities.OrderBy(c => c.Name).ToList();
        }

        public static List<Product> GetProducts(int companyId,bool sw)
        {
            var products = db.Products.Where(c => c.CompanyId == companyId).ToList();
            return products.OrderBy(c => c.Description).ToList();
        }

        public static List<Order> GetOrders(int companyId, bool v)
        {
            var orders = db.Orders.Where(c => c.CompanyId == companyId && c.State.Description == "Created").ToList();
            return orders.OrderBy(c => c.OrderId).ToList();
        }

        public static List<Product> GetProducts(int companyId)
        {
            var products = db.Products.Where(c => c.CompanyId == companyId).ToList();
            products.Add(new Product { ProductId = 0, Description = "[Select a product...]" });
            return products.OrderBy(c => c.Description).ToList();
        }

        public static List<Company> GetCompanies(int departmentId, int cityId)
        {
            var companies = db.Companies.Where(c => c.DepartmentId == departmentId && c.CityId == cityId).ToList();
            companies.Add(new Company { CompanyId = 0, Name = "[Select a company...]" });
            return companies.OrderBy(c => c.Name).ToList();
        }

        public static List<Category> GetCategories(int companyId)
        {
            var category = db.Categories.Where(c => c.CompanyId == companyId).ToList();
            category.Add(new Category { CategoryId = 0, Description = "[Select a category...]" });
            return category.OrderBy(c => c.Description).ToList();
        }

        public static List<Customer> GetCustomers(int companyId)
        {
            var qry = (from cu in db.Customers
                       join cc in db.CompanyCustomers on cu.CustomerId equals cc.CustomerId
                       join co in db.Companies on cc.CompanyId equals co.CompanyId
                       where co.CompanyId == companyId
                       select new { cu }).ToList();

            var customers = new List<Customer>();
            foreach (var item in qry)
            {
                customers.Add(item.cu);
            }
            customers.Add(new Customer { CustomerId = 0, FirstName = "[Select a customer...]" });
            return customers.OrderBy(c => c.FirstName).ThenBy(c => c.LastName).ToList();
        }

        public static List<Supplier> GetSuppliers(int companyId)
        {
            var qry = (from su in db.Suppliers
                       join sc in db.SupplierCompanies on su.SupplierId equals sc.SupplierId
                       join co in db.Companies on sc.CompanyId equals co.CompanyId
                       where co.CompanyId == companyId
                       select new { su }).ToList();

            var suppliers = new List<Supplier>();
            foreach (var item in qry)
            {
                suppliers.Add(item.su);
            }
            suppliers.Add(new Supplier { SupplierId = 0, FirstName = "[Select a supplier...]" });
            return suppliers.OrderBy(c => c.FirstName).ThenBy(c => c.LastName).ToList();
        }

        public static List<Warehouse> GetWarehouses(int companyId)
        {
            var warehouses = db.Warehouses.Where(c => c.CompanyId == companyId).ToList();
            warehouses.Add(new Warehouse { WarehouseId = 0, Name = "[Select a warehouse...]" });
            return warehouses.OrderBy(c => c.Name).ToList();
        }

        public static List<Tax> GetTaxes(int companyId)
        {
            var taxes = db.Taxes.Where(c => c.CompanyId == companyId).ToList();
            taxes.Add(new Tax { TaxId = 0, Description = "[Select a tax...]" });
            return taxes.OrderBy(c => c.Description).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}