﻿using Ecomerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ecomerce.Controllers
{
    public class GenericController : Controller
    {
        private EcomerceContext db = new EcomerceContext();

        public JsonResult GetCities(int departmentId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cities = db.Cities.Where(c => c.DepartmentId == departmentId).OrderBy(o => o.Name);
            return Json(cities);
        }

        public JsonResult GetCompanies(int departmentId, int cityId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var companies = db.Companies.Where(c => c.DepartmentId == departmentId && c.CityId == cityId).OrderBy(o => o.Name);
            return Json(companies);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}