﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ecomerce.Models
{
    public class SupplierCompany
    {
        [Key]
        public int SupplierCompanyId { get; set; }

        public int SupplierId { get; set; }
        public int CompanyId { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual Company Company { get; set; }
    }
}