﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ecomerce.Models;

namespace Ecomerce.Classes
{
    public class DBHelper
    {
        public static Response SaveChanges(EcomerceContext db)
        {
            try
            {
                db.SaveChanges();
                return new Response { Succeeded = true, };
            }
            catch (Exception ex)
            {
                var response = new Response { Succeeded = false, };
                if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("_Index"))
                {
                    response.Message = "There is a record with the same value";
                }
                else if (ex.InnerException != null &&
                    ex.InnerException.InnerException != null &&
                    ex.InnerException.InnerException.Message.Contains("REFERENCE"))
                {
                    response.Message = "The record can't be delete because it has related records";
                }
                else
                {
                    response.Message = ex.Message;
                }

                return response;
            }
        }

        public static Response OrderInvoiced(EcomerceContext db,int companyId,int orderId)
        {
            var sale = db.Sales.Where(s => s.CompanyId == companyId && s.OrderId == orderId).FirstOrDefault();
            if (sale != null)
            {
                var response = new Response { Succeeded = false, };
                response.Message = "The record can't be delete because it has related records";
                return response;
            }
            else
            {
                return new Response { Succeeded = true, };
            }
        }

        public static int GetState(string descripcion, EcomerceContext db)
        {
            var state = db.States.Where(s => s.Description == descripcion).FirstOrDefault();
            if (state == null)
            {
                state = new State { Description = descripcion, };
                db.States.Add(state);
                db.SaveChanges();
            }

            return state.StateId;
        }

        public static int GetOrder(string descripcion, EcomerceContext db)
        {
            var state = db.States.Where(s => s.Description == descripcion).FirstOrDefault();
            if (state == null)
            {
                state = new State { Description = descripcion, };
                db.States.Add(state);
                db.SaveChanges();
            }

            return state.StateId;
        }
    }
}